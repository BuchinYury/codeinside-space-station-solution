package ru.codeinside.spacestation;

public class JeanLucPicard implements Captain {

    private final Spaceship spaceship;

    private int amountRooms;

    public JeanLucPicard(Spaceship spaceship) {
        this.spaceship = spaceship;
    }

    @Override
    public int stateroomsNumber() {

        spaceship.current().turnOn();
        int iter = 0;

        while (true) {
            spaceship.next();
            iter++;

            if (spaceship.current().isLightOn()) {
                spaceship.current().turnOff();

                amountRooms = iter;

                while (iter > 0) {
                    spaceship.previous();
                    iter--;
                }

                if (!spaceship.current().isLightOn()) break;
            }
        }

        return amountRooms;
    }
}
